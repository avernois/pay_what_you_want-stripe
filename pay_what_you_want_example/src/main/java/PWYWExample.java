import fr.craftinglabs.apps.paywhatyouwant.core.model.Amount;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Product;
import fr.craftinglabs.apps.paywhatyouwant.core.model.ProductName;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Quantity;
import fr.craftinglabs.apps.paywhatyouwant.server.PayWhatYouWantServer;

public class PWYWExample {

    public static void main(String[] args) throws InterruptedException {
        String stripeToken = "YOUR_PRIVATE_STRIPE_TOKEN";
        int port = 4567;

        Product exampleProduct = new Product(ProductName.fromString("example"), Amount.fromCents(4500));
        Product limitedQuantityProduct = new Product(ProductName.fromString("another product"), Amount.fromCents(4500), Quantity.fromInt(10));

        PayWhatYouWantServer server = PayWhatYouWantServer.aServer()
                .forProduct(limitedQuantityProduct, exampleProduct)
                .withStripeToken(stripeToken)
                .onCancelRedirectTo("https://my.cancel.url")
                .onSuccessRedirectTo("https://my.cancel.url")
                .onPort(port)
                .build();

        server.start();
    }
}
