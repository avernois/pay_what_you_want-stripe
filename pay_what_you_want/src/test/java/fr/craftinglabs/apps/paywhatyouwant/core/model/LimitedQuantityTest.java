package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class LimitedQuantityTest extends QuantityTest {
    @Test public void
    should_have_a_quantity() {
        LimitedQuantity limitedQuantity = LimitedQuantity.fromInt(12);

        assertThat(limitedQuantity.asInt(), is(12));
    }

    @Test public void
    should_decrement_quantity() {
        LimitedQuantity limitedQuantity = LimitedQuantity.fromInt(12);

        assertThat(limitedQuantity.decrement(), is(LimitedQuantity.fromInt(11)));
    }

    @Test public void
    should_increment_quantity() {
        LimitedQuantity limitedQuantity = LimitedQuantity.fromInt(12);

        assertThat(limitedQuantity.increment(), is(LimitedQuantity.fromInt(13)));
    }

    @Override
    public void should_be_positive_when_above_zero() {
        LimitedQuantity limitedQuantity = LimitedQuantity.fromInt(42);

        assertTrue(limitedQuantity.strictlyPositive());
    }

    @Override
    public void should_not_be_positive_when_equals_or_below_zero() {
        LimitedQuantity limitedQuantity = LimitedQuantity.fromInt(0);

        assertFalse(limitedQuantity.strictlyPositive());
    }
}
