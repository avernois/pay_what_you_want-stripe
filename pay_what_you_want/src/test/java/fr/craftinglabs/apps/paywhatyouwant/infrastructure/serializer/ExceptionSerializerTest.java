package fr.craftinglabs.apps.paywhatyouwant.infrastructure.serializer;

import fr.craftinglabs.apps.paywhatyouwant.core.action.BelowMinimumAmount;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Amount;
import fr.craftinglabs.apps.paywhatyouwant.core.model.ProductName;
import fr.craftinglabs.apps.paywhatyouwant.core.service.NoneToSell;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentProcessingException;
import fr.craftinglabs.apps.paywhatyouwant.core.service.UnknownProductName;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExceptionSerializerTest {
    @Test public void
    should_return_serialized_version_of_BelowAmountException() {
        BelowMinimumAmount exception = new BelowMinimumAmount(Amount.fromCents(10), Amount.fromCents(100));

        assertEquals("{\"type\":\"BELOW_MINIMUM_AMOUNT\",\"amount\":{\"amountInCents\":10},\"minimumAmount\":{\"amountInCents\":100}}", ExceptionSerializer.toJson(exception));
    }

    @Test public void
    should_retrun_serialized_version_of_payment_processing_Exception() {
        PaymentProcessingException exception = new PaymentProcessingException();

        assertEquals("{\"type\":\"PAYMENT_PROCESSING_EXCEPTION\"}", ExceptionSerializer.toJson(exception));
    }

    @Test public void
    should_return_serialized_version_of_unknown_product_name(){
        UnknownProductName exception = new UnknownProductName(ProductName.fromString("Azalé"));

        assertEquals("{\"type\":\"UNKNOWN_PRODUCT_NAME\",\"productName\":\"Azalé\"}", ExceptionSerializer.toJson(exception));
    }

    @Test public void
    should_return_serialized_version_of_none_to_sell(){
        NoneToSell exception = new NoneToSell();

        assertEquals("{\"type\":\"NONE_TO_SELL\"}", ExceptionSerializer.toJson(exception));
    }


    @Test public void
    should_return_serialized_on_unknown_exception(){
        Exception exception = new Exception();

        assertEquals("{\"type\":\"UNKNOWN_EXCEPTION\"}", ExceptionSerializer.toJson(exception));
    }
}
