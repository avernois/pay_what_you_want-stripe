package fr.craftinglabs.apps.paywhatyouwant.core.service;

import org.junit.Before;
import org.junit.Test;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Amount;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Product;
import fr.craftinglabs.apps.paywhatyouwant.core.model.ProductName;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Quantity;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class ProductServiceTest {

    private ProductService service;

    private static final Product PRODUCT_UNLIMITED = new Product(ProductName.fromString("nom"), Amount.fromEurosAsString("5"));
    public static final Product PRODUCT_LIMITED_TO_ONE = new Product(ProductName.fromString("LimitedProduct"), Amount.fromCents(500), Quantity.fromInt(1));

    @Before
    public void setUp() throws Exception {
        List<Product> products = asList(PRODUCT_UNLIMITED, PRODUCT_LIMITED_TO_ONE);
        service = new ProductService(products);
    }

    @Test public void
    should_return_an_existing_product() throws UnknownProductName {
        Product product = service.get(ProductName.fromString("nom"));

        assertThat(product, is(PRODUCT_UNLIMITED));
    }

    @Test(expected = UnknownProductName.class) public void
    should_throw_an_exception_when_asked_product_does_not_exist() throws UnknownProductName {
        service.get(ProductName.fromString("unknown_product"));
    }

    @Test public void
    should_remove_an_existing_product() {
        service.remove(PRODUCT_UNLIMITED.name());

        assertFalse(isPresent());
    }

    @Test(expected = NoneToSell.class) public void
    should_throw_an_exception_when_there_are_no_more_product_to_sell() throws NoneToSell {
        service.hold(PRODUCT_LIMITED_TO_ONE.name());

        service.hold(PRODUCT_LIMITED_TO_ONE.name());
    }

    @Test public void
    should_accept_to_hold_a_product_when_there_are_enough_to_sell() throws NoneToSell {
        service.hold(PRODUCT_LIMITED_TO_ONE.name());
    }

    @Test public void
    should_accept_to_hold_an_unlimited_product() throws NoneToSell {
        service.hold(PRODUCT_UNLIMITED.name());
    }

    @Test public void
    should_accept_to_hold_a_product_once_released() throws NoneToSell {
        service.hold(PRODUCT_LIMITED_TO_ONE.name());

        service.release(PRODUCT_LIMITED_TO_ONE.name());

        service.hold(PRODUCT_LIMITED_TO_ONE.name());
    }

    @Test public void
    should_accept_to_release_an_unlimited_product() {
        service.release(PRODUCT_UNLIMITED.name());
    }


    private boolean isPresent() {
        boolean isPresent = true;
        try {
            service.get(PRODUCT_UNLIMITED.name());
        } catch (UnknownProductName unknownProductName) {
            isPresent = false;
        }
        return isPresent;
    }
}