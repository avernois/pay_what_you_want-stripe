package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CheckoutSessionIdTest {

    @Test public void
    should_return_the_id_as_a_string() {
        CheckoutSessionId checkoutid = CheckoutSessionId.fromString("A SESSION ID");

        assertThat(checkoutid.toString(), is("A SESSION ID"));
    }
}