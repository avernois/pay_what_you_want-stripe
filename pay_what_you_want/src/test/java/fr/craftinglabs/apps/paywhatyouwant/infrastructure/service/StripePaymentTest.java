package fr.craftinglabs.apps.paywhatyouwant.infrastructure.service;

import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import com.stripe.param.checkout.SessionCreateParams.ShippingAddressCollection.AllowedCountry;
import fr.craftinglabs.apps.paywhatyouwant.core.model.*;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentProcessingException;
import org.junit.Assume;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class StripePaymentTest {

    private static final ProductDescription PRODUCT_DESCRIPTION = ProductDescription.fromString("a description");
    private static final Product A_PRODUCT = new Product(ProductName.fromString("a product"), Amount.fromCents(500), PRODUCT_DESCRIPTION);
    private String stripeTestToken;
    private StripePayment service;

    private void assumeStripeTokenIsSet() {
        stripeTestToken = System.getenv("STRIPE_TEST_TOKEN");
        Assume.assumeThat("STRIPE_TEST_TOKEN env variable is not set", stripeTestToken, notNullValue());
    }

    @Test public void
    should_create_a_checkout_session() throws PaymentProcessingException {
        assumeStripeTokenIsSet();
        service = new StripePayment(stripeTestToken, new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        CheckoutSessionId id = service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.empty());

        assertNotNull(id);
    }

    @Test public void
    should_call_stripe_with_given_success_url() throws PaymentProcessingException {
        CallSpyingFakeStripePayment service = new CallSpyingFakeStripePayment("a token", new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.empty());
        service.assertSuccessUrlIs("https://my.success.url");

    }

    @Test public void
    should_call_stripe_with_url_success_at_checkout() throws PaymentProcessingException {
        CallSpyingFakeStripePayment service = new CallSpyingFakeStripePayment("a token", new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.of(new RedirectUrls("https://another.success.url","https://another.cancel.url")));

        service.assertSuccessUrlIs("https://another.success.url");
    }

    @Test public void
    should_call_stripe_with_given_cancel_url() throws PaymentProcessingException {
        CallSpyingFakeStripePayment service = new CallSpyingFakeStripePayment("a token", new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.empty());

        service.assertCancelUrlIs("https://my.cancel.url");
    }

    @Test public void
    should_call_stripe_with_url_cancel_at_checkout() throws PaymentProcessingException {
        CallSpyingFakeStripePayment service = new CallSpyingFakeStripePayment("a token", new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.of(new RedirectUrls("https://another.success.url", "https://another.cancel.url")));

        service.assertCancelUrlIs("https://another.cancel.url");
    }

    @Test public void
    should_call_stripe_with_product_description() throws PaymentProcessingException {
        CallSpyingFakeStripePayment service = new CallSpyingFakeStripePayment("a token", new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.empty());

        service.assertItemHasDescription(PRODUCT_DESCRIPTION.toString());
    }

    @Test public void
    should_call_stripe_with_allowed_country_list() throws PaymentProcessingException {
        CallSpyingFakeStripePayment service = new CallSpyingFakeStripePayment("a token", new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.empty());

        service.assertCollectShippingAddressForAllowedCountries(asList("AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SI", "ES", "SE",
                "IS", "LI", "NO",
                "CH", "GB"));
    }

    @Test public void
    should_call_stripe_with_price_set_in_euro() throws PaymentProcessingException {
        CallSpyingFakeStripePayment service = new CallSpyingFakeStripePayment("a token", new RedirectUrls("https://my.success.url", "https://my.cancel.url"));

        service.checkout(A_PRODUCT, Amount.fromCents(100), Optional.empty());

        service.assertItemHasCurrency("eur");
    }

    class CallSpyingFakeStripePayment extends StripePayment {

        private SessionCreateParams params;

        CallSpyingFakeStripePayment(String token, RedirectUrls urls) {
            super(token, urls);
        }

        @Override
        protected Session getStripeSession(SessionCreateParams params) {
            this.params = params;
            return new Session();
        }

        @Override
        protected void setStripeApiKey(String privateKey) {
            // do nothing here yet
        }

        public void assertItemHasDescription(String productDescription) {
            for(SessionCreateParams.LineItem item: params.getLineItems()) {
                System.out.println(item.getDescription());
                assertThat(item.getDescription().toString(), is(productDescription));
            }
        }

        public void assertSuccessUrlIs(String successUrl) {
            assertThat(params.getSuccessUrl(), is(successUrl));
        }

        public void assertCancelUrlIs(String cancelUrl) {
            assertThat(params.getCancelUrl(), is(cancelUrl));
        }

        public void assertItemHasCurrency(String currency) {
            for(SessionCreateParams.LineItem item: params.getLineItems()) {
                assertThat(item.getPriceData().getCurrency(), is(currency));
            }
        }

        public void assertCollectShippingAddressForAllowedCountries(List<String> asList) {
            List<AllowedCountry> allowedCountries = params.getShippingAddressCollection().getAllowedCountries();
            assertEquals(asList.size(), allowedCountries.size());
            for(AllowedCountry country: allowedCountries) {
                assertThat(asList, hasItem(country.getValue()));
            }
        }
    }
}

