package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ProductDescriptionTest {
    @Test public void
    should_have_a_String_representation() {
        ProductDescription description = ProductDescription.fromString("a description");

        assertThat(description.toString(), is("a description"));
    }

}
