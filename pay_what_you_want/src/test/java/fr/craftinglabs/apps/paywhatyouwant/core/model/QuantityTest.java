package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


abstract public class QuantityTest {

    @Test abstract public void
    should_decrement_quantity();

    @Test abstract public void
    should_increment_quantity();

    @Test abstract public void
    should_be_positive_when_above_zero();

    @Test abstract public void
    should_not_be_positive_when_equals_or_below_zero();

    @Test public void
    should_return_an_unlimited_quantity() {
        Quantity unlimited = Quantity.unlimited();

        assertThat(unlimited, is(UnlimitedQuantity.unlimited()));
    }

    @Test public void
    should_return_a_limited_quantity() {
        Quantity limited = Quantity.fromInt(42);

        assertThat(limited, is(LimitedQuantity.fromInt(42)));
    }

}
