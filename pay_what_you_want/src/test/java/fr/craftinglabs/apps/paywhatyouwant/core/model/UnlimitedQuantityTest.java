package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class UnlimitedQuantityTest extends QuantityTest {

    @Test public void
    should_decrement_quantity() {
        UnlimitedQuantity unlimitedQuantity = UnlimitedQuantity.unlimited();

        assertThat(unlimitedQuantity.decrement(), is(UnlimitedQuantity.unlimited()));
    }

    @Test public void
    should_increment_quantity() {
        UnlimitedQuantity unlimitedQuantity = UnlimitedQuantity.unlimited();

        assertThat(unlimitedQuantity.increment(), is(UnlimitedQuantity.unlimited()));
    }

    @Override
    public void should_be_positive_when_above_zero() {
        UnlimitedQuantity unlimitedQuantity = UnlimitedQuantity.unlimited();

        assertTrue(unlimitedQuantity.strictlyPositive());
    }

    @Override
    public void should_not_be_positive_when_equals_or_below_zero() {
        // Unlimited can't be equals or below zero (or there is something very very wrong)
    }

}