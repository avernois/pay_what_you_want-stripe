package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ProductTest {

    @Test public void
    should_have_a_name() {
        Product product = new Product(ProductName.fromString("un nom"), Amount.fromCents(500));

        assertThat(product.name(), is(ProductName.fromString("un nom")));
    }

    @Test public void
    should_have_a_minimum_price() {
        Product product = new Product(ProductName.fromString("un nom"), Amount.fromCents(500));

        assertThat(product.minimumPrice(), is(Amount.fromCents(500)));
    }

    @Test public void
    should_have_a_maximum_number_of_item_to_sell() {
        Product product = new Product(ProductName.fromString("un nom"), Amount.fromCents(500), LimitedQuantity.fromInt(10));

        assertThat(product.maximumToSell(), is(LimitedQuantity.fromInt(10)));
    }

    @Test public void
    should_have_an_empty_maximum_of_item_to_sell_when_none_are_given() {
        Product product = new Product(ProductName.fromString("un nom"), Amount.fromCents(500));

        assertThat(product.maximumToSell(), is(UnlimitedQuantity.unlimited()));
    }

    @Test public void
    should_have_a_description_when_one_is_given() {
        Product product = new Product(ProductName.fromString("un nom"), Amount.fromCents(500), ProductDescription.fromString("la description"));

        assertThat(product.description(), is(Optional.of(ProductDescription.fromString("la description"))));
    }

    @Test public void
    should_have_an_empty_description_when_none_is_given() {
        Product product = new Product(ProductName.fromString("un nom"), Amount.fromCents(500));

        assertThat(product.description(), is(Optional.empty()));
    }


}
