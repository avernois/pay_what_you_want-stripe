package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ProductNameTest {

    @Test public void
    should_have_a_String_representation() {
        ProductName productName = ProductName.fromString("a toString");

        assertThat(productName.toString(), is("a toString"));
    }

}
