package fr.craftinglabs.apps.paywhatyouwant.server;

import fr.craftinglabs.apps.paywhatyouwant.core.action.BelowMinimumAmount;
import fr.craftinglabs.apps.paywhatyouwant.core.action.CreateCheckoutSession;
import fr.craftinglabs.apps.paywhatyouwant.core.model.*;
import fr.craftinglabs.apps.paywhatyouwant.core.service.NoneToSell;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentProcessingException;
import fr.craftinglabs.apps.paywhatyouwant.core.service.UnknownProductName;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ServerTest {

    public static final int SERVER_PORT = 5678;
    private Server server;
    private CreateCheckoutSession checkoutSession;

    @Before
    public void setUp() throws PaymentProcessingException, UnknownProductName, NoneToSell, BelowMinimumAmount {
        checkoutSession = mock(CreateCheckoutSession.class);
        when(checkoutSession.checkout(any(), any(), any())).thenReturn(CheckoutSessionId.fromString("A SESSION ID"));
        server = new Server(checkoutSession, SERVER_PORT);

        server.start();
    }

    @After
    public void tearDown() {
        server.stop();
    }

    @Test public void
    should_create_a_checkout_session_for_a_given_product() throws IOException, BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        HttpResponse response =
                HttpPostBuilder.aCheckout("productName", 5)
                        .execute();

        assertThat(response.getStatusLine(). getStatusCode(), is(200));
        assertThat(getContentAsString(response), is("A SESSION ID"));
        verify(checkoutSession).checkout(ProductName.fromString("productName"), Amount.fromCents(500), Optional.empty());
    }
    @Test public void
    should_create_a_checkout_session_for_a_given_product_with_succesUrl_and_cancelUrl() throws IOException, BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        String successUrl = "https://success.url";
        String cancelUrl = "https://cancel.url";
        HttpResponse response =
                HttpPostBuilder.aCheckout("productName", 5)
                        .withParam("success_url", successUrl)
                        .withParam("cancel_url", cancelUrl)
                        .execute();

        assertThat(response.getStatusLine(). getStatusCode(), is(200));
        assertThat(getContentAsString(response), is("A SESSION ID"));
        verify(checkoutSession).checkout(ProductName.fromString("productName"), Amount.fromCents(500), Optional.of(new RedirectUrls(successUrl, cancelUrl)));
    }

    @Test public void
    should_return_a_400_error_when_amount_is_below_minimum_for_a_checkout() throws IOException, BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        doThrow(new BelowMinimumAmount(Amount.fromCents(500), Amount.fromCents(1000))).when(checkoutSession).checkout(any(), any(), any());
        HttpResponse response =
                HttpPostBuilder.aCheckout("productName", 1)
                        .execute();

        assertThat(response.getStatusLine().getStatusCode(), is(400));
        assertThat(getContentAsString(response), is("{\"type\":\"BELOW_MINIMUM_AMOUNT\",\"amount\":{\"amountInCents\":500},\"minimumAmount\":{\"amountInCents\":1000}}"));
    }

    @Test public void
    should_return_a_404_error_when_asking_to_checkout_an_unexisting_product() throws IOException, BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        doThrow(new UnknownProductName(ProductName.fromString("unknownProduct"))).when(checkoutSession).checkout(any(), any(), any());
        HttpResponse response =
                HttpPostBuilder.aCheckout("unknownProduct", 1)
                        .execute();

        assertThat(response.getStatusLine().getStatusCode(), is(404));
        assertThat(getContentAsString(response), is("{\"type\":\"UNKNOWN_PRODUCT_NAME\",\"productName\":\"unknownProduct\"}"));
    }

    @Test public void
    should_return_a_410_error_when_there_are_no_product_to_checkout() throws IOException, BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        doThrow(new NoneToSell()).when(checkoutSession).checkout(any(), any(), any());
        HttpResponse response =
                HttpPostBuilder.aCheckout("productName", 1)
                        .execute();

        assertThat(response.getStatusLine().getStatusCode(), is(410));
        assertThat(getContentAsString(response), is("{\"type\":\"NONE_TO_SELL\"}"));
    }

    @Test public void
    should_return_a_400_error_when_there_is_a_problem_in_the_checkout_process() throws IOException, BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        doThrow(new PaymentProcessingException()).when(checkoutSession).checkout(any(), any(), any());
        HttpResponse response =
                HttpPostBuilder.aCheckout("productname", 5)
                        .execute();

        assertThat(response.getStatusLine().getStatusCode(), is(400));
        assertThat(getContentAsString(response), is("{\"type\":\"PAYMENT_PROCESSING_EXCEPTION\"}"));
    }

    @Test public void
    should_return_CORS_header_when_enabled() throws IOException, BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        server.enableCORS("myAllowedOrigin");
        server.start();

        HttpOptions request = new HttpOptions("http://localhost:" + SERVER_PORT + "/checkout/");
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(request);

        assertThat(response.getFirstHeader("Access-Control-Allow-Origin").getValue(), is("myAllowedOrigin"));
    }

    private String getContentAsString(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    private static class HttpPostBuilder {


        List<NameValuePair> params = new ArrayList<>();
        private String URI;


        public static HttpPostBuilder aCheckout(String productName, int i) {
            HttpPostBuilder builder = new HttpPostBuilder();
            builder.toURI("http://localhost:" + SERVER_PORT + "/checkout/" + productName)
                    .withParam("amount", "5");
            return builder;
        }

        public HttpPostBuilder toURI(String URI) {
            this.URI = URI;
            return this;
        }

        public HttpPostBuilder withParam(String name, String value) {
            params.add(new BasicNameValuePair(name, value));

            return this;
        }


        public HttpResponse execute() throws IOException {

            HttpPost request = new HttpPost(URI);
            request.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpClient client = HttpClientBuilder.create().build();

            return client.execute(request);
        }
    }
}