package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CardTokenTest {
    @Test public void
    should_return_token_as_string() {
        CardToken token = CardToken.fromString("tok_visa");

        assertEquals("tok_visa", token.toString());
    }
}