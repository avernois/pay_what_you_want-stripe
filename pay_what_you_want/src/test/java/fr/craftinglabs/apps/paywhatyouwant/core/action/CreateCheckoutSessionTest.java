package fr.craftinglabs.apps.paywhatyouwant.core.action;

import fr.craftinglabs.apps.paywhatyouwant.core.model.*;
import fr.craftinglabs.apps.paywhatyouwant.core.service.*;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CreateCheckoutSessionTest {
    private PaymentService service;
    public static final Product PRODUCT_UNLIMITED = new Product(ProductName.fromString("a toString"), Amount.fromCents(500));
    public static final Product PRODUCT_DEPLETED = new Product(ProductName.fromString("DepletedProduct"), Amount.fromCents(500), Quantity.fromInt(0));
    public static final List<Product> PRODUCTS = asList(PRODUCT_UNLIMITED, PRODUCT_DEPLETED);
    private ProductService productService;

    @Before
    public void setUp() {
        service = mock(PaymentService.class);
        productService = new ProductService(PRODUCTS);
    }

    @Test public void
    should_create_a_checkout_session() throws PaymentProcessingException, UnknownProductName, BelowMinimumAmount, NoneToSell {
        CreateCheckoutSession createCheckoutSession = new CreateCheckoutSession(productService, service);
        Amount amount = Amount.fromCents(500);
        when(service.checkout(PRODUCT_UNLIMITED, amount, Optional.empty())).thenReturn(CheckoutSessionId.fromString("A SESSION ID"));

        CheckoutSessionId id = createCheckoutSession.checkout(PRODUCT_UNLIMITED.name(), amount, Optional.empty());

        assertEquals(CheckoutSessionId.fromString("A SESSION ID"), id);
        verify(service).checkout(PRODUCT_UNLIMITED, amount, Optional.empty());
    }

    @Test public void
    should_create_a_checkout_session_with_successUrl_and_cancelUrl_parameters() throws PaymentProcessingException, UnknownProductName, BelowMinimumAmount, NoneToSell {
        CreateCheckoutSession createCheckoutSession = new CreateCheckoutSession(productService, service);
        Amount amount = Amount.fromCents(500);
        RedirectUrls urls = new RedirectUrls("https://another.succes.url", "https://another.cancel.url");
        when(service.checkout(PRODUCT_UNLIMITED, amount, Optional.of(urls))).thenReturn(CheckoutSessionId.fromString("A SESSION ID"));

        CheckoutSessionId id = createCheckoutSession.checkout(PRODUCT_UNLIMITED.name(), amount, Optional.of(urls));

        verify(service).checkout(PRODUCT_UNLIMITED, amount, Optional.of(urls));
        assertEquals(CheckoutSessionId.fromString("A SESSION ID"), id);
    }

    @Test(expected = BelowMinimumAmount.class) public void
    should_throw_an_exception_when_amount_is_below_minimum_price() throws BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        CreateCheckoutSession createCheckoutSession = new CreateCheckoutSession(productService, service);

        createCheckoutSession.checkout(PRODUCT_UNLIMITED.name(), Amount.fromCents(400), Optional.empty());
    }

    @Test(expected = NoneToSell.class) public void
    should_throw_an_exception_when_there_are_no_products_to_sell() throws BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        CreateCheckoutSession createCheckoutSession = new CreateCheckoutSession(productService, service);

        createCheckoutSession.checkout(PRODUCT_DEPLETED.name(), Amount.fromCents(500), Optional.empty());
    }

    @Test(expected = UnknownProductName.class) public void
    should_throw_an_exception_when_product_does_not_exist() throws BelowMinimumAmount, PaymentProcessingException, UnknownProductName, NoneToSell {
        CreateCheckoutSession createCheckoutSession = new CreateCheckoutSession(productService, service);

        createCheckoutSession.checkout(ProductName.fromString("Unknown product"), Amount.fromCents(500), Optional.empty());
    }

    @Test public void
    should_release_the_product_when_payment_failed() throws PaymentProcessingException, BelowMinimumAmount, UnknownProductName, NoneToSell {
        productService = mock(ProductService.class);
        when(productService.get(PRODUCT_UNLIMITED.name())).thenReturn(PRODUCT_UNLIMITED);
        doThrow(new PaymentProcessingException()).when(service).checkout(any(), any(), any());
        CreateCheckoutSession createCheckoutSession = new CreateCheckoutSession(productService, service);

        try {
            createCheckoutSession.checkout(PRODUCT_UNLIMITED.name(), Amount.fromCents(500), Optional.empty());
            fail();
        } catch (PaymentProcessingException e) {
            verify(productService).release(PRODUCT_UNLIMITED.name());
        }
    }

    @Test public void
    should_not_release_the_product_when_paiement_succeeded() throws PaymentProcessingException, BelowMinimumAmount, UnknownProductName, NoneToSell {
        productService = mock(ProductService.class);
        when(productService.get(PRODUCT_UNLIMITED.name())).thenReturn(PRODUCT_UNLIMITED);
        CreateCheckoutSession createCheckoutSession = new CreateCheckoutSession(productService, service);

        createCheckoutSession.checkout(PRODUCT_UNLIMITED.name(), Amount.fromCents(500), Optional.empty());

        verify(productService, never()).release(PRODUCT_UNLIMITED.name());
    }
}