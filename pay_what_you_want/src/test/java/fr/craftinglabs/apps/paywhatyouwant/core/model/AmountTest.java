package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class AmountTest {
    @Test public void
    should_return_the_amount_in_cents() {
        Amount amount = Amount.fromCents(500);

        assertEquals(500, amount.inCents());
    }

    @Test public void
    should_be_created_from_a_string_representing_an_amount_in_cents() {
        Amount amount = Amount.fromCentsAsString("500");

        assertEquals(500, amount.inCents());
    }

    @Test public void
    should_be_created_from_a_string_representing_an_amount_in_euros() {
        Amount amount = Amount.fromEurosAsString("5");

        assertEquals(500, amount.inCents());
    }

    @Test(expected = NumberFormatException.class) public void
    should_throw_an_exception_if_amount_does_not_represent_an_integer() {
        Amount.fromCentsAsString("500.5");
    }

    @Test public void
    should_tell_when_the_amount_is_below_another() {
        Amount amount = Amount.fromCents(500);
        Amount biggerAmount = Amount.fromCents(1000);

        assertTrue(amount.isStrictlyBelow(biggerAmount));
    }

    @Test public void
    should_not_tell_it_is_below_when_the_amount_are_the_same() {
        Amount amount = Amount.fromCents(500);
        Amount sameAmount = Amount.fromCents(500);

        assertFalse(amount.isStrictlyBelow(sameAmount));
    }

    @Test public void
    should_tell_when_the_amount_is_not_below_another() {
        Amount amount = Amount.fromCents(500);
        Amount smallerAmount = Amount.fromCents(250);

        assertFalse(amount.isStrictlyBelow(smallerAmount));
    }
}