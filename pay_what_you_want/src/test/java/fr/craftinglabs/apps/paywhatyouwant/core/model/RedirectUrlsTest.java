package fr.craftinglabs.apps.paywhatyouwant.core.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class RedirectUrlsTest {

    @Test public void
    should_return_the_success_url() {
        RedirectUrls urls = new RedirectUrls("https://success.url", "https://cancel.url");

        assertEquals("https://success.url", urls.success());
    }

    @Test public void
    should_return_the_cancel_url() {
        RedirectUrls urls = new RedirectUrls("https://success.url", "https://cancel.url");

        assertEquals("https://cancel.url", urls.cancel());
    }

    @Test(expected = IllegalArgumentException.class) public void
    should_return_error_when_url_success_is_null() {
        new RedirectUrls(null, "https://cancel.url");
    }

    @Test (expected = IllegalArgumentException.class) public void
    should_throw_an_error_if_success_url_is_not_valid() {
        new RedirectUrls("This_is_not_an_url", "https://cancel.url");
    }

    @Test (expected = IllegalArgumentException.class) public void
    should_throw_an_error_if_cancel_url_is_not_valid() {
        new RedirectUrls("https://success.url", "This_is_not_an_url");
    }
}
