package fr.craftinglabs.apps.paywhatyouwant.core.model;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class RedirectUrls {
    private String cancel;
    private String success;

    public RedirectUrls(String success, String cancel) {
        try {
            new URL(success);
            new URL(cancel);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException();
        }

        this.cancel = cancel;
        this.success = success;
    }

    public String success() {
        return success;
    }

    public String cancel() {
        return cancel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RedirectUrls url = (RedirectUrls) o;
        return Objects.equals(cancel, url.cancel) &&
                Objects.equals(success, url.success);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cancel, success);
    }
}
