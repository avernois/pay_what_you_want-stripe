package fr.craftinglabs.apps.paywhatyouwant.core.service;

import fr.craftinglabs.apps.paywhatyouwant.core.model.Product;
import fr.craftinglabs.apps.paywhatyouwant.core.model.ProductName;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Quantity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ProductService {

    private final Map<ProductName, Product> products = new HashMap<>();
    private final Map<ProductName, Quantity> stock = new ConcurrentHashMap<>();

    public ProductService(List<Product> products) {
        for(Product product : products) {
            this.products.put(product.name(), product);
            stock.put(product.name(), product.maximumToSell());
        }
    }

    public Product get(ProductName productName) throws UnknownProductName {
        if (!products.containsKey(productName)) {
            throw  new UnknownProductName(productName);
        }
        return products.get(productName);
    }

    public void remove(ProductName name) {
        products.remove(name);
    }

    synchronized public void hold(ProductName name) throws NoneToSell {
        Quantity quantity = stock.get(name);

        if(!quantity.strictlyPositive()) {
            throw new NoneToSell();
        }

        stock.put(name, quantity.decrement());
    }

    synchronized public void release(ProductName name) {
        Quantity quantity = stock.get(name);
        stock.put(name, quantity.increment());
    }
}
