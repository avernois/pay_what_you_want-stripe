package fr.craftinglabs.apps.paywhatyouwant.core.model;

import java.util.Objects;
import java.util.Optional;

public class Product {
    final private ProductName name;
    final private Amount minimumPrice;
    final private Optional<ProductDescription> description;
    final private Quantity maximumToSell;

    public Product(ProductName name, Amount minimumPrice) {
        this(name, minimumPrice, UnlimitedQuantity.unlimited(), Optional.empty());
    }

    public Product(ProductName name, Amount minimumPrice, Quantity maximumToSell) {
        this(name, minimumPrice, maximumToSell, Optional.empty());
    }

    public Product(ProductName name, Amount minimumPrice, ProductDescription description) {
        this(name, minimumPrice, UnlimitedQuantity.unlimited(), Optional.of(description));
    }

    public Product(ProductName name, Amount minimumPrice, Quantity maximumToSell, ProductDescription description) {
        this(name, minimumPrice, maximumToSell, Optional.of(description));
    }

    private Product(ProductName name, Amount minimumPrice, Quantity maximumToSell, Optional<ProductDescription> description) {
        this.name = name;
        this.minimumPrice = minimumPrice;
        this.description = description;
        this.maximumToSell = maximumToSell;
    }

    public ProductName name() {
        return name;
    }

    public Amount minimumPrice() {
        return minimumPrice;
    }

    public Quantity maximumToSell() {
        return maximumToSell;
    }

    public Optional<ProductDescription> description() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) &&
                Objects.equals(minimumPrice, product.minimumPrice) &&
                Objects.equals(description, product.description) &&
                Objects.equals(maximumToSell, product.maximumToSell);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, minimumPrice, description, maximumToSell);
    }
}
