package fr.craftinglabs.apps.paywhatyouwant.core.action;

import fr.craftinglabs.apps.paywhatyouwant.core.model.*;
import fr.craftinglabs.apps.paywhatyouwant.core.service.*;

import java.util.Optional;

public class CreateCheckoutSession {
    private final ProductService productService;
    private final PaymentService paymentService;

    public CreateCheckoutSession(ProductService productService, PaymentService paymentService) {
        this.productService = productService;
        this.paymentService = paymentService;
    }

    public CheckoutSessionId checkout(ProductName productName, Amount amount, String successUrl, String cancelUrl) throws PaymentProcessingException, BelowMinimumAmount, UnknownProductName, NoneToSell {
        return checkout(productName, amount, Optional.of(new RedirectUrls(successUrl, cancelUrl)));
    }

    public CheckoutSessionId checkout(ProductName productName, Amount amount, Optional<RedirectUrls> redirectUrl) throws PaymentProcessingException, BelowMinimumAmount, UnknownProductName, NoneToSell {
        Product product = productService.get(productName);

        if (amount.isStrictlyBelow(product.minimumPrice())) {
            throw new BelowMinimumAmount(amount, product.minimumPrice());
        }

        productService.hold(product.name());

        CheckoutSessionId id;
        try {
            id = paymentService.checkout(product, amount, redirectUrl);
        } catch (PaymentProcessingException paymentException) {
            productService.release(product.name());
            throw paymentException;
        }

        return id;
    }
}
