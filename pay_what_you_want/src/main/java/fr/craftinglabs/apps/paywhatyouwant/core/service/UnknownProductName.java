package fr.craftinglabs.apps.paywhatyouwant.core.service;

import fr.craftinglabs.apps.paywhatyouwant.core.model.ProductName;

public class UnknownProductName extends Exception {
    private ProductName unknownProductName;

    public UnknownProductName(ProductName unknownProductName) {

        this.unknownProductName = unknownProductName;
    }

    public ProductName unknownProductName() {
        return unknownProductName;
    }
}
