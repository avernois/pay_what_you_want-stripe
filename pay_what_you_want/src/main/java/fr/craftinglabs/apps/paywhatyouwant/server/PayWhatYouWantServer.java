package fr.craftinglabs.apps.paywhatyouwant.server;

import fr.craftinglabs.apps.paywhatyouwant.core.action.CreateCheckoutSession;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Product;
import fr.craftinglabs.apps.paywhatyouwant.core.model.RedirectUrls;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentService;
import fr.craftinglabs.apps.paywhatyouwant.core.service.ProductService;
import fr.craftinglabs.apps.paywhatyouwant.infrastructure.service.StripePayment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PayWhatYouWantServer {
    private Server server;

    public void start() throws InterruptedException {
        server.start();
        addShutdownHook(server);
        Thread.currentThread().join();
    }

    private PayWhatYouWantServer(Server server) {
        this.server = server;
    }

    private static void addShutdownHook(Server server) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Exiting.");
            server.stop();
        }, "shutdownHook"));
    }

    public static ServerBuilder aServer() {
        return new ServerBuilder();
    }

    public static class ServerBuilder {
        private final List<Product> products = new ArrayList<>();
        private final List<String> origins = new ArrayList<>();
        private int port = 80;
        private String stripeToken;
        private String cancelUrl;
        private String successUrl;

        public ServerBuilder() {
        }

        public ServerBuilder withStripeToken(String stripeToken) {
            this.stripeToken = stripeToken;
            return this;
        }

        public ServerBuilder forProduct(Product ...products) {
            for(Product product:products) {
                this.products.add(product);
            }
            return this;
        }

        public ServerBuilder onPort(int port) {
            this.port = port;
            return this;
        }

        public ServerBuilder allowingCorsOrigin(String ...origins) {
            for(String origin: origins) {
                this.origins.add(origin);
            }
            return this;
        }

        public ServerBuilder onCancelRedirectTo(String cancelUrl) {
            this.cancelUrl = cancelUrl;
            return this;
        }

        public ServerBuilder onSuccessRedirectTo(String successUrl) {
            this.successUrl = successUrl;
            return this;
        }

        public PayWhatYouWantServer build() {
            PaymentService paymentService = new StripePayment(stripeToken, new RedirectUrls(successUrl, cancelUrl));
            ProductService productService = new ProductService(products);

            CreateCheckoutSession checkoutSession = new CreateCheckoutSession(productService, paymentService);

            Server server = new Server(checkoutSession, port);

            if(!origins.isEmpty()) {
                server.enableCORS(origins.stream().collect(Collectors.joining(",")));
            }

            return new PayWhatYouWantServer(server);
        }
    }
}


