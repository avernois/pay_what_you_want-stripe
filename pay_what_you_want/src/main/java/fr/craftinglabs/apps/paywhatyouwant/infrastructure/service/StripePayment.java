package fr.craftinglabs.apps.paywhatyouwant.infrastructure.service;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import com.stripe.param.checkout.SessionCreateParams.ShippingAddressCollection.AllowedCountry;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Amount;
import fr.craftinglabs.apps.paywhatyouwant.core.model.CheckoutSessionId;
import fr.craftinglabs.apps.paywhatyouwant.core.model.Product;
import fr.craftinglabs.apps.paywhatyouwant.core.model.RedirectUrls;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentProcessingException;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;

public class StripePayment implements PaymentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StripePayment.class);
    private final RedirectUrls defaultUrls;

    public StripePayment(String privateKey, RedirectUrls redirectUrls) {
        setStripeApiKey(privateKey);
        defaultUrls = redirectUrls;
    }

    @Override
    public CheckoutSessionId checkout(Product product, Amount amount, Optional<RedirectUrls> urls) throws PaymentProcessingException {
        List<AllowedCountry> europeanCountries = asList(
                AllowedCountry.AT,
                AllowedCountry.BE,
                AllowedCountry.BG,
                AllowedCountry.HR,
                AllowedCountry.CY,
                AllowedCountry.CZ,
                AllowedCountry.DK,
                AllowedCountry.EE,
                AllowedCountry.FI,
                AllowedCountry.FR,
                AllowedCountry.DE,
                AllowedCountry.GR,
                AllowedCountry.HU,
                AllowedCountry.IE,
                AllowedCountry.IT,
                AllowedCountry.LV,
                AllowedCountry.LT,
                AllowedCountry.LU,
                AllowedCountry.MT,
                AllowedCountry.NL,
                AllowedCountry.PL,
                AllowedCountry.PT,
                AllowedCountry.RO,
                AllowedCountry.SK,
                AllowedCountry.SI,
                AllowedCountry.ES,
                AllowedCountry.SE);

        List<AllowedCountry> europeanZoneCountries = asList(
                AllowedCountry.IS,
                AllowedCountry.LI,
                AllowedCountry.NO);

        List<AllowedCountry> otherCountries = asList(
                AllowedCountry.GB,
                AllowedCountry.CH);

        SessionCreateParams params =
            SessionCreateParams.builder()
            .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
            .setMode(SessionCreateParams.Mode.PAYMENT)
            .addLineItem(
                SessionCreateParams.LineItem.builder()
                .setQuantity(1L)
                .setPriceData(
                    SessionCreateParams.LineItem.PriceData.builder()
                    .setCurrency("eur")
                    .setUnitAmount(Long.valueOf(amount.inCents()))
                    .setProductData(
                        SessionCreateParams.LineItem.PriceData.ProductData.builder()
                        .setName(product.name().toString())
                        .build())
                    .build())
                .setDescription(product.description().map(d -> d.toString()).orElse("no description provided"))
                .build())
            .setShippingAddressCollection(
                    SessionCreateParams.ShippingAddressCollection.builder()
                    .addAllAllowedCountry(europeanCountries)
                    .addAllAllowedCountry(europeanZoneCountries)
                    .addAllAllowedCountry(otherCountries)
                    .build())
            .setSuccessUrl(urls.map(u -> u.success()).orElse(defaultUrls.success()))
            .setCancelUrl(urls.map(u -> u.cancel()).orElse(defaultUrls.cancel()))
            .build();

        Session session;
        try {
            session = getStripeSession(params);
        } catch (StripeException e) {
            LOGGER.error("Checkout: Request to stripe failed: {}", e.getStackTrace());
            throw new PaymentProcessingException(e);
        }
        return CheckoutSessionId.fromString(session.getId());
    }

    protected Session getStripeSession(SessionCreateParams params) throws StripeException {
        return Session.create(params);
    }

    protected void setStripeApiKey(String privateKey) {
        Stripe.apiKey = privateKey;
    }
}
