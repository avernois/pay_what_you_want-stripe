package fr.craftinglabs.apps.paywhatyouwant.core.model;

import java.util.Objects;

public class CheckoutSessionId {
    private String id;

    private CheckoutSessionId(String id) {
        this.id = id;
    }

    public static CheckoutSessionId fromString(String id) {
        return new CheckoutSessionId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckoutSessionId that = (CheckoutSessionId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id;
    }
}
