package fr.craftinglabs.apps.paywhatyouwant.core.service;

public class PaymentProcessingException extends Exception {

    public PaymentProcessingException() {
        super();
    }

    public PaymentProcessingException(Exception e) {
        super(e);
    }
}
