package fr.craftinglabs.apps.paywhatyouwant.core.model;

import java.util.Objects;

abstract public class Quantity {

    public static Quantity unlimited() {
        return UnlimitedQuantity.unlimited();
    }

    public static Quantity fromInt(Integer quantity) {
        return LimitedQuantity.fromInt(quantity);
    }

    abstract public Quantity decrement();

    abstract public Quantity increment();

    abstract public boolean strictlyPositive();

}

class UnlimitedQuantity extends Quantity {

    private static final UnlimitedQuantity UNLIMITED_QUANTITY = new UnlimitedQuantity();

    private UnlimitedQuantity() { }

    public static UnlimitedQuantity unlimited() {
        return UNLIMITED_QUANTITY;
    }

    public UnlimitedQuantity decrement() {
        return UNLIMITED_QUANTITY;
    }

    public UnlimitedQuantity increment() {
        return UNLIMITED_QUANTITY;
    }

    @Override
    public boolean strictlyPositive() {
        return true;
    }
}

class LimitedQuantity extends Quantity {
    private Integer quantity;

    protected LimitedQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public static LimitedQuantity fromInt(Integer quantity) {
        return new LimitedQuantity(quantity);
    }

    public Integer asInt() {
        return quantity;
    }

    public LimitedQuantity decrement() {
        return LimitedQuantity.fromInt(this.quantity -1);
    }

    public LimitedQuantity increment() {
        return LimitedQuantity.fromInt(this.quantity + 1);
    }

    @Override
    public boolean strictlyPositive() {
        return quantity > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LimitedQuantity limitedQuantity1 = (LimitedQuantity) o;

        return Objects.equals(quantity, limitedQuantity1.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity);
    }
}
