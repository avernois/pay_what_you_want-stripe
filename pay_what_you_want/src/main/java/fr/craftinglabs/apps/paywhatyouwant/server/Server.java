package fr.craftinglabs.apps.paywhatyouwant.server;

import fr.craftinglabs.apps.paywhatyouwant.core.action.BelowMinimumAmount;
import fr.craftinglabs.apps.paywhatyouwant.core.action.CreateCheckoutSession;
import fr.craftinglabs.apps.paywhatyouwant.core.model.*;
import fr.craftinglabs.apps.paywhatyouwant.core.service.NoneToSell;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentProcessingException;
import fr.craftinglabs.apps.paywhatyouwant.core.service.UnknownProductName;
import fr.craftinglabs.apps.paywhatyouwant.infrastructure.serializer.ExceptionSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.Optional;

import static spark.Spark.*;

public class Server {

    private final CreateCheckoutSession checkoutSession;

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    public Server(CreateCheckoutSession checkoutSession, int port) {
        this.checkoutSession = checkoutSession;
        port(port);
    }

    public void start() {
        post("/checkout/:productname", ((request, response) -> {
            ProductName productName = ProductName.fromString(request.params((":productname")));
            String amount = request.queryParams("amount");
            LOGGER.info("Checkout: product={}, amount={}", productName, amount);
            Optional<RedirectUrls> redirectUrls = extractRedirectUrls(request);

            CheckoutSessionId checkoutSessionId;
            checkoutSessionId = checkoutSession.checkout(productName, Amount.fromEurosAsString(amount), redirectUrls);

            return checkoutSessionId.toString();
        }));

        exception(BelowMinimumAmount.class, (belowMinimunAmount, request, response) -> {
            LOGGER.error("BelowMinimumAmount: amount={}, minimumAmount={}", belowMinimunAmount.amount(), belowMinimunAmount.minimumAmount() );
            if(redirect(request, response, "redirectOnBelowAmount"))
                return;

            response.status(400);
            response.body(ExceptionSerializer.toJson(belowMinimunAmount));
        });

        exception(PaymentProcessingException.class, (paymentProcessingException, request, response) -> {
            LOGGER.error("PaymentProcessingException: {}", paymentProcessingException.getMessage());
            if(redirect(request, response, "redirectOnPaymentProcess"))
                return;

            response.status(400);
            response.body(ExceptionSerializer.toJson(paymentProcessingException));
        });

        exception(UnknownProductName.class, (unknownProductName, request, response) -> {
            LOGGER.error("UnknownProductName: {}", unknownProductName.unknownProductName().toString());
            if(redirect(request, response, "redirectOnUnexistingProduct"))
                return;

            response.status(404);
            response.body(ExceptionSerializer.toJson(unknownProductName));
        });

        exception(NoneToSell.class, (noneToSell, request, response) -> {
            LOGGER.error("{}\n\tproduct name: {}", noneToSell.getMessage(), request.params(":productname"));
            if(redirect(request, response, "redirectOnNoneToSell"))
                return;

            response.status(410);
            response.body(ExceptionSerializer.toJson(noneToSell));
        });

        exception(Exception.class, (exception, request, response) -> {
            LOGGER.error("UnknownException: {}", exception.getStackTrace());
            if(redirect(request, response, "redirectOnUnknownException"))
                return;

            response.status(500);
            response.body(ExceptionSerializer.toJson(exception));
        });
        awaitInitialization();
    }

    private Optional<RedirectUrls> extractRedirectUrls(Request request) {
        String successUrl = request.queryParams("success_url");
        String cancelUrl = request.queryParams("cancel_url");
        Optional<RedirectUrls> redirectUrls;
        if(successUrl == null || cancelUrl == null) {
            redirectUrls = Optional.empty();
        } else {
            redirectUrls = Optional.of(new RedirectUrls(successUrl, cancelUrl));
        }
        return redirectUrls;
    }

    private boolean redirect(Request request, Response response, String redirectParam) {
        Optional<String> redirectURL = Optional.ofNullable(request.queryParams(redirectParam));
        redirectURL.ifPresent(url -> {
            LOGGER.error("Redirect to: {}", url);
            response.redirect(url);
        });

        return redirectURL.isPresent();
    }


    public void stop() {
        Spark.stop();
        Spark.awaitStop();
    }

    public void enableCORS(final String origin) {

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
        });
    }
}
