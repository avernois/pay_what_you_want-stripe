package fr.craftinglabs.apps.paywhatyouwant.infrastructure.serializer;

import fr.craftinglabs.apps.paywhatyouwant.core.action.BelowMinimumAmount;
import fr.craftinglabs.apps.paywhatyouwant.core.service.NoneToSell;
import fr.craftinglabs.apps.paywhatyouwant.core.service.PaymentProcessingException;
import fr.craftinglabs.apps.paywhatyouwant.core.service.UnknownProductName;

public class ExceptionSerializer {
    public static String toJson(BelowMinimumAmount exception) {
        return "{\"type\":\"BELOW_MINIMUM_AMOUNT\",\"amount\":{\"amountInCents\":" + exception.amount().inCents() +
                "},\"minimumAmount\":{\"amountInCents\":" + exception.minimumAmount().inCents() +
                "}}";
    }

    public static String toJson(PaymentProcessingException exception) {
        return "{\"type\":\"PAYMENT_PROCESSING_EXCEPTION\"}";
    }

    public static String toJson(UnknownProductName exception) {
        return "{\"type\":\"UNKNOWN_PRODUCT_NAME\",\"productName\":\"" + exception.unknownProductName().toString() + "\"}";
    }

    public static String toJson(NoneToSell exception) {
        return "{\"type\":\"NONE_TO_SELL\"}";
    }

    public static String toJson(Exception exception) {
        return "{\"type\":\"UNKNOWN_EXCEPTION\"}";
    }
}
