package fr.craftinglabs.apps.paywhatyouwant.core.service;

import fr.craftinglabs.apps.paywhatyouwant.core.model.*;

import java.util.Optional;

public interface PaymentService {
    CheckoutSessionId checkout(Product product, Amount amount, Optional<RedirectUrls> urls) throws PaymentProcessingException;
}
