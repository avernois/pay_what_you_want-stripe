package fr.craftinglabs.apps.paywhatyouwant.core.model;

import java.util.Objects;

public class ProductDescription {
    private String description;

    private ProductDescription(String description) {

        this.description = description;
    }

    public static ProductDescription fromString(String description) {
        return new ProductDescription(description);
    }

    @Override
    public String toString() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDescription that = (ProductDescription) o;
        return Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }
}
