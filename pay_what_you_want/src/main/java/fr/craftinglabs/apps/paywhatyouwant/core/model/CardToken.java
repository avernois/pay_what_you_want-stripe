package fr.craftinglabs.apps.paywhatyouwant.core.model;

import java.util.Objects;

public class CardToken {
    private String token;

    private CardToken(String token) {
        this.token = token;
    }

    public static CardToken fromString(String token) {
        return new CardToken(token);
    }

    @Override
    public String toString() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardToken cardToken = (CardToken) o;
        return Objects.equals(token, cardToken.token);
    }

    @Override
    public int hashCode() {

        return Objects.hash(token);
    }
}
