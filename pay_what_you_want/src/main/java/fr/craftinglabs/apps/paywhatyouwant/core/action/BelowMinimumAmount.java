package fr.craftinglabs.apps.paywhatyouwant.core.action;

import fr.craftinglabs.apps.paywhatyouwant.core.model.Amount;

public class BelowMinimumAmount extends Exception {

    private final Amount amount;
    private final Amount minimumAmount;

    public BelowMinimumAmount(Amount amount, Amount minimumAmount) {

        this.amount = amount;
        this.minimumAmount = minimumAmount;
    }

    public Amount amount() {
        return amount;
    }

    public Amount minimumAmount() {
        return minimumAmount;
    }
}
