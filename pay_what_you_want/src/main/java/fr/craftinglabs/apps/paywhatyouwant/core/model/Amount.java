package fr.craftinglabs.apps.paywhatyouwant.core.model;

import java.util.Objects;

public class Amount {

    private int amountInCents;

    private Amount(int amountInCents) {
        this.amountInCents = amountInCents;
    }

    public static Amount fromCents(int amountInCents) {
        return new Amount(amountInCents);
    }

    public static Amount fromCentsAsString(String amountInCents) {
        return new Amount(Integer.valueOf(amountInCents));
    }

    public static Amount fromEurosAsString(String amountInEuros) {
        return new Amount(Integer.valueOf(amountInEuros)*100);
    }

    public int inCents() {
        return amountInCents;
    }

    public boolean isStrictlyBelow(Amount amount) {
        return (amountInCents < amount.amountInCents);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Amount amount = (Amount) o;
        return amountInCents == amount.amountInCents;
    }

    @Override
    public int hashCode() {

        return Objects.hash(amountInCents);
    }

    @Override
    public String toString() {
        return "Amount{" +
                "amountInCents=" + amountInCents +
                '}';
    }
}
